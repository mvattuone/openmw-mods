# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Imperial Houses and Forts Retexture - Ordo Arkitektora"
    DESC = "Full retexture for houses and castles of imperial colonists in Vvardenfell"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/43940
        https://www.nexusmods.com/morrowind/mods/45336
    """
    KEYWORDS = "openmw"
    # permission obtained by marius851000
    LICENSE = "all-rights-reserved-mirror-permission"
    TEXTURE_SIZES = "1024 2048"
    NEXUS_SRC_URI = """
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/43940?tab=files&file_id=1000004588
            -> Ordo_Arkitektora_MQ-43940-1-0.7z
        )
        texture_size_2048? (
            https://www.nexusmods.com/morrowind/mods/43940?tab=files&file_id=1000004587
            -> Ordo_Arkitektora_HQ-43940-1-0.7z
        )
        map_normal? (
            https://www.nexusmods.com/morrowind/mods/45336?tab=files&file_id=1000021693
            -> 01b_Imperial_wood_(Ordo_Arkitektora)-45336-1-0-1605035259.7z
        )
    """
    # Retex-Patch for Windoors Glow
    # Windoors_Glow_OA_MQ-43940-1-0.7z
    # Windoors_Glow_OA_HQ-43940-1-0.7z
    # Replacers for AOFs Imperial Houses BumpMapped
    # Bump_Mapped_OA_MQ-43940-1-0.7z
    # Bump_Mapped_OA_HQ-43940-1-0.7z
    IUSE = "map_normal"
    INSTALL_DIRS = [
        InstallDir(
            "MQ/Data Files",
            S="Ordo_Arkitektora_MQ-43940-1-0",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "HQ Ordo Arkitektora/Data Files",
            S="Ordo_Arkitektora_HQ-43940-1-0",
            REQUIRED_USE="texture_size_2048",
        ),
        # Retex-Patch for Windoors Glow
        # InstallDir(
        #     "MQ Windoors Glow Arkitektora/Data Files",
        #     S="Windoors_Glow_OA_MQ-43940-1-0"
        # ),
        # InstallDir(
        #     "HQ Windoors Glow Arkitektora/Data Files",
        #     S="Windoors_Glow_OA_HQ-43940-1-0"
        # ),
        # Patches for AOFs Imperial Houses BumpMapped
        # InstallDir(
        #    "MQ Imperial Arkitektora BumpMapped/Data Files",
        #    S="Bump_Mapped_OA_MQ-43940-1-0",
        #    REQUIRED_USE="texture_size_1024 map_normal",
        # ),
        # InstallDir(
        #     "HQ Imperial Arkitektora BumpMapped/Data Files",
        #     S="Bump_Mapped_OA_HQ-43940-1-0",
        #     REQUIRED_USE="texture_size_2048 map_normal",
        # ),
        InstallDir(
            "01b Imperial wood (Ordo Arkitektora)",
            S="01b_Imperial_wood_(Ordo_Arkitektora)-45336-1-0-1605035259",
            REQUIRED_USE="map_normal",
        ),
    ]
